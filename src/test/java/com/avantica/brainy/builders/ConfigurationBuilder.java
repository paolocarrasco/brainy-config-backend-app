package com.avantica.brainy.builders;

import com.avantica.brainy.model.Configuration;

public class ConfigurationBuilder {
    private String key = "a key";
    private String value = "a value";

    public Configuration build() {
        return new Configuration(key, value);
    }
}