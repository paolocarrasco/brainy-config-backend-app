#!/bin/sh

docker build -t brainy/config-backend-dev .
docker create --name configuration_db redis
docker create -it -p 8080:8080 --link configuration_db -v $(pwd):/var/www --name config_backend_dev brainy/config-backend-dev 
